# Danmake
## What is Danmake?
Danmake is an engine to create bullet hell (danmaku) games. Similar to danmakufu, but more easy.

## Status
Danmake can be used to make levels as of right now. But, there is no documentation, there are bugs probably (lots of them, i haven't tested it much) and features are missing. And the code sucks.

## What is it made with?
Danmake is developed in Rust. It uses `ggez` 2D game framework for most of the code, along with `specs` for ECS and `serde` for deserialization.

## How to run it
0. Make sure you have `cargo` and `git`.
1. `git clone https://gitlab.com/yusdacra/danmake/ && cd danmake`
2. `cargo run --release` or `cargo run` for debug mode. First build can be long.

## Future features
* 3D support, and customizing the background with patterns and colors
* Level chaining ("if player finishes this level, run that level afterwards")
* Ability to load compressed levels
* Replay support (probably one of the most important features)
* Scoreboard
* Scripting support (likely, and probably asap)