use ggez;
use ggez::graphics::{self, spritebatch::SpriteBatch};
use ggez_goodies::scene;
use specs::{self, world::WorldExt, Builder, Join};

use crate::components as c;
use crate::input;
use crate::scenes;
use crate::systems::*;
use crate::types::*;
use crate::user_types::*;
use crate::utils;
use crate::world::World;

pub struct LevelScene {
    done: bool,
    dispatcher: specs::Dispatcher<'static, 'static>,
    sprbatch: SpriteBatch,
}

impl LevelScene {
    pub fn new(ctx: &mut ggez::Context, world: &mut World, level_name: &str) -> Self {
        let done = false;
        // Actually load level (setups things like sprite handler etc.)
        world
            .load_level(level_name, ctx)
            .expect("An error occured while loading level");

        let dispatcher = Self::register_systems();
        LevelScene {
            done,
            dispatcher,
            sprbatch: SpriteBatch::new(
                graphics::Image::solid(ctx, 10, graphics::Color::new(1.0, 1.0, 1.0, 1.0)).unwrap(),
            ),
        }
    }

    fn register_systems() -> specs::Dispatcher<'static, 'static> {
        specs::DispatcherBuilder::new()
            .with(MaintainLevel::default(), "maintain", &[])
            .with(MoveVelocity, "apply_velocity", &[])
            .with(BulletRemove, "check_bullets", &["apply_velocity"])
            .with(ExecuteAction, "execute_act", &["maintain"])
            .with(
                CollideBullets,
                "collide_bullets",
                &["apply_velocity", "execute_act"],
            )
            .build()
    }
}

pub fn create_player(gameworld: &mut World, player_data: PlayerData) {
    let sprid = *gameworld
        .specs_world
        .fetch::<SpriteMap>()
        .get(&player_data.sprite)
        .unwrap();
    let sprsize = gameworld.specs_world.fetch::<SpriteHandler>().get(&sprid).0;
    gameworld
        .specs_world
        .create_entity()
        .with(c::Player {
            score: 0,
            speed: player_data.speed,
            lives: player_data.lives,
            max_lives: player_data.max_lives,
        })
        .with(sprid)
        .with(c::Bounds::new(
            player_data.spawn_position.into(),
            player_data.hitbox_size.unwrap_or(sprsize.into()).into(),
        ))
        .with(c::Motion::default())
        .build();
}

impl scene::Scene<World, input::Event> for LevelScene {
    fn update(&mut self, gameworld: &mut World, _ctx: &mut ggez::Context) -> scenes::Switch {
        // Player logic
        for (motion, bd, player) in (
            &mut gameworld.specs_world.write_storage::<c::Motion>(),
            &gameworld.specs_world.read_storage::<c::Bounds>(),
            &gameworld.specs_world.read_storage::<c::Player>(),
        )
            .join()
        {
            // Update player velocity
            motion.0 = utils::math::vec2(
                gameworld.input.get_axis_raw(input::Axis::Horz),
                gameworld.input.get_axis_raw(input::Axis::Vert),
            ) * DESIRED_DELTA as f32
                * player.speed;
            // Check if player pressed the fire button, if so fire a bullet from player's weapon(s)
            if gameworld.input.get_button_down(input::Button::Fire) {}
        }
        // Dispatch le systems (and stuff)
        self.dispatcher.dispatch(&gameworld.specs_world);
        // Maintain world so lazily created entities actually persist
        gameworld.specs_world.maintain();
        // Pop level if we are done
        // TODO: This should go to game over if player is dead, credits if game is finished (consider making an enum)
        if self.done {
            scene::SceneSwitch::Pop
        } else {
            scene::SceneSwitch::None
        }
    }

    fn draw(&mut self, gameworld: &mut World, ctx: &mut ggez::Context) -> ggez::GameResult<()> {
        use crate::render::*;
        // Render background
        render_background(gameworld, ctx)?;
        // Render entities (wow very informative comment)
        render_entities(gameworld, ctx, &mut self.sprbatch)?;
        // Render player hitbox
        if gameworld.input.get_button_down(input::Button::Focus) {
            render_player_hitbox(gameworld, ctx)?;
        }
        Ok(())
    }

    fn name(&self) -> &str {
        "LevelScene"
    }

    fn input(&mut self, gameworld: &mut World, ev: input::Event, started: bool) {
        gameworld.input.update_effect(ev, started);
    }
}
