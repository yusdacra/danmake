use ggez;
use ggez::graphics::{self, spritebatch::SpriteBatch};
use ggez_goodies::scene;

use crate::components as c;
use crate::input;
use crate::scenes;
use crate::systems::*;
use crate::types::*;
use crate::world::World;

pub struct LevelSelectScene {
    done: bool,
    dispatcher: specs::Dispatcher<'static, 'static>,
}

impl LevelSelectScene {
    pub fn new(ctx: &mut ggez::Context, world: &mut World) -> Self {
        let done = false;
        let dispatcher = Self::register_systems();
        LevelSelectScene { done, dispatcher }
    }

    fn register_systems() -> specs::Dispatcher<'static, 'static> {
        specs::DispatcherBuilder::new().build()
    }
}

impl scene::Scene<World, input::Event> for LevelSelectScene {
    fn update(&mut self, gameworld: &mut World, ctx: &mut ggez::Context) -> scenes::Switch {
        scene::SceneSwitch::None
    }
    fn draw(&mut self, gameworld: &mut World, ctx: &mut ggez::Context) -> ggez::GameResult<()> {
        Ok(())
    }
    fn name(&self) -> &str {
        "LevelSelectScene"
    }
    fn input(&mut self, gameworld: &mut World, ev: input::Event, started: bool) {
        gameworld.input.update_effect(ev, started);
    }
}
