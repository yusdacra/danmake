use ggez::graphics::{draw, spritebatch::SpriteBatch, DrawParam};
use ggez::{Context, GameResult};
use specs::{Join, WorldExt};

use crate::components as c;
use crate::types::{SpriteHandler, SpriteMap};
use crate::user_types::LevelData;
use crate::world::World;

#[inline(always)]
pub fn render_entities(
    gameworld: &mut World,
    ctx: &mut Context,
    sprbatch: &mut SpriteBatch,
) -> GameResult<()> {
    let sprites = gameworld.specs_world.fetch::<SpriteHandler>();
    // Get ready for drawing
    for id in 0..sprites.len() {
        // Clear old sprite data
        sprbatch.clear();
        // Set sprite
        let (size, img) = sprites.get(&c::SpriteIdx(id));
        sprbatch.set_image(img.clone());
        for (sprid, bd) in (
            &gameworld.specs_world.read_storage::<c::SpriteIdx>(),
            &gameworld.specs_world.read_storage::<c::Bounds>(),
        )
            .join()
        {
            if sprid.0 == id {
                // Add sprite data back again
                sprbatch.add(DrawParam::default().dest(bd.center.to_point() + (*size * -0.5)));
            }
        }
        // Actually draw the batch
        draw(ctx, sprbatch, DrawParam::default())?;
    }
    Ok(())
}

#[inline(always)]
pub fn render_player_hitbox(gameworld: &mut World, ctx: &mut Context) -> GameResult<()> {
    let sprref = gameworld.specs_world.fetch::<SpriteHandler>();
    let sprmapref = gameworld.specs_world.fetch::<SpriteMap>();
    let sprid = sprmapref
        .get(
            if let Some(w) = &gameworld
                .specs_world
                .fetch::<LevelData>()
                .player_hitbox_sprite
            {
                w
            } else {
                return Ok(());
            },
        )
        .unwrap();
    for (_, bd) in (
        &gameworld.specs_world.read_storage::<c::Player>(),
        &gameworld.specs_world.read_storage::<c::Bounds>(),
    )
        .join()
    {
        draw(
            ctx,
            &sprref.get(sprid).1,
            DrawParam::default().dest(bd.center.to_point() + (sprref.get(sprid).0 * -0.5)),
        )?;
    }
    Ok(())
}

#[inline(always)]
pub fn render_background(gameworld: &mut World, ctx: &mut Context) -> GameResult<()> {
    draw(
        ctx,
        &gameworld
            .specs_world
            .fetch::<SpriteHandler>()
            .get(
                gameworld
                    .specs_world
                    .fetch::<SpriteMap>()
                    .get(
                        if let Some(w) = &gameworld.specs_world.fetch::<LevelData>().bg_sprite {
                            w
                        } else {
                            return Ok(());
                        },
                    )
                    .unwrap(),
            )
            .1,
        DrawParam::default(),
    )?;
    Ok(())
}
