use specs::*;
use specs_derive::*;

use crate::types::*;
use crate::user_types::*;
use crate::utils::Vector2;

/// Bounds of an entity (position + size) (also used for collision checks) (if both extent axes are zero then this can't be collided)
#[derive(Debug, Clone, Component)]
#[storage(VecStorage)]
pub struct Bounds {
    pub center: Vector2,
    pub extents: Vector2,
}

impl Bounds {
    pub fn new(center: Vector2, size: Vector2) -> Self {
        Bounds {
            center,
            extents: size * 0.5,
        }
    }

    pub fn min(&self) -> Vector2 {
        self.center - self.extents
    }

    pub fn max(&self) -> Vector2 {
        self.center + self.extents
    }

    pub fn size(&self) -> Vector2 {
        self.extents * 2.0
    }

    #[inline(always)]
    pub fn overlaps(&self, bounds: &Bounds) -> bool {
        ((self.center.x - bounds.center.x).abs() < self.extents.x + bounds.extents.x)
            && ((self.center.y - bounds.center.y).abs() < self.extents.y + bounds.extents.y)
    }

    #[inline(always)]
    pub fn contains(&self, point: Vector2) -> bool {
        (point.x - self.center.x).abs() <= self.extents.x
            && (point.y - self.center.y) <= self.extents.y
    }
}

#[derive(Clone, Debug, Component, Default)]
#[storage(VecStorage)]
pub struct Motion(pub Vector2);

/// Sprite ids.
#[derive(Clone, Copy, Debug, Component)]
#[storage(VecStorage)]
pub struct SpriteIdx(pub usize);

/// A queue which holds actions of an enemy.
#[derive(Clone, Debug, Component)]
#[storage(DenseVecStorage)]
pub struct ActionQueue {
    actions: ActionLayout,
    timer: Timer,
    current_actions: Vec<(Action, Timer)>,
}

impl ActionQueue {
    pub fn new(actions: ActionLayout) -> Self {
        ActionQueue {
            actions,
            timer: Timer::zero(),
            current_actions: Vec::new(),
        }
    }
    pub fn update(&mut self) {
        for (_, timer) in &mut self.current_actions {
            timer.tick();
        }
        if let Some(actions) = self.actions.remove(&self.timer.get_time_secs()) {
            for actdata in actions {
                self.current_actions.push((actdata, Timer::zero()));
            }
        }
        self.timer.tick();
    }
    pub fn mut_curacts(&mut self) -> &mut Vec<(Action, Timer)> {
        &mut self.current_actions
    }
}

#[derive(Clone, Debug, Component)]
#[storage(DenseVecStorage)]
pub struct PlayerBullet {
    pub damage: u64,
}

#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct EnemyBullet {
    pub damage: u64,
}

/// Enemy marker and data.
#[derive(Clone, Debug, Component)]
#[storage(DenseVecStorage)]
pub struct Enemy {
    pub lives: u64,
    pub score_gain: u64,
}

/// Player marker and data.
#[derive(Clone, Debug, Component)]
#[storage(HashMapStorage)]
pub struct Player {
    pub max_lives: u64,
    pub lives: u64,
    pub score: u64,
    pub speed: f32,
}

pub fn register_components(specs_world: &mut World) {
    // Data
    specs_world.register::<Bounds>();
    specs_world.register::<Motion>();
    specs_world.register::<SpriteIdx>();
    specs_world.register::<ActionQueue>();

    // Markers
    specs_world.register::<Player>();
    specs_world.register::<Enemy>();
    specs_world.register::<PlayerBullet>();
    specs_world.register::<EnemyBullet>();
}
