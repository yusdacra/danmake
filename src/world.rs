use crate::{components, input};

use specs::{self, world::WorldExt};

use std::path;

pub struct World {
    pub resource_root: path::PathBuf,
    pub input: input::State,
    pub specs_world: specs::World,
}

impl World {
    pub fn new(resource_dir: path::PathBuf) -> Self {
        let mut w = specs::World::new();
        components::register_components(&mut w);

        Self {
            resource_root: resource_dir,
            input: input::State::new(),
            specs_world: w,
        }
    }

    pub fn load_level(&mut self, name: &str, ctx: &mut ggez::Context) -> ggez::GameResult<()> {
        use crate::types::{SpriteHandler, SpriteMap};
        use crate::user_types::{
            BulletTypes, EnemyTypes, Layout, LevelData, LevelMetadata, PlayerData,
        };
        log::info!("Loading level: {}", name);
        // Clear everything (also overwrite sprite handler)
        log::debug!(
            "getting ready to load level {}, cleaning up everything!",
            name
        );
        log::debug!("resource directory is {:?}", self.resource_root);
        self.specs_world.delete_all();
        self.specs_world.insert(SpriteHandler::new());
        self.specs_world.insert(SpriteMap::new());

        let level_folder = self.resource_root.join(format!("levels/{}/", name));

        // Load metadata
        let metadata: LevelMetadata = serde_any::from_file_stem(level_folder.join("level-info"))
            .expect("Uh oh, could not load level metadata. Now crashing!!");
        log::info!("{}", metadata);

        // Load level data
        let data: LevelData = serde_any::from_file_stem(level_folder.join("level-data"))
            .expect("Uh oh, could not load level data. Now crashing!!");
        // Add background
        self.specs_world.insert(data);

        // Load the sprites
        let mut sprite_handler = self.specs_world.fetch_mut::<SpriteHandler>();
        let mut sprite_map = self.specs_world.fetch_mut::<SpriteMap>();
        let sprite_dir = "sprites/";
        log::debug!("loading every sprite in {:?}", sprite_dir);
        for entry in std::fs::read_dir(level_folder.join(sprite_dir))
            .expect("Uh oh, no sprite directory found. Now crashing!!")
        {
            let spr_name = entry
                .expect("Couldn't get dir entry. Now crashing!!")
                .file_name()
                .to_string_lossy()
                .into_owned();

            log::debug!("loading sprite: {}", spr_name);
            let img =
                ggez::graphics::Image::new(ctx, format!("/levels/{}/sprites/{}", name, spr_name))?;
            sprite_map.insert(spr_name.into_boxed_str(), sprite_handler.add(img));
        }

        std::mem::drop(sprite_handler);
        std::mem::drop(sprite_map);

        let layout: Layout = serde_any::from_file_stem(level_folder.join("layout"))
            .expect("Uh oh, could not load level layout. Now crashing!!");
        self.specs_world.insert(layout);
        let bullet_types: BulletTypes =
            serde_any::from_file_stem(level_folder.join("bullet-types"))
                .expect("Uh oh, could not load bullet types. Now crashing!!");
        self.specs_world.insert(bullet_types);
        let enemy_types: EnemyTypes = serde_any::from_file_stem(level_folder.join("enemy-types"))
            .expect("Uh oh, could not load enemy types. Now crashing!!");
        self.specs_world.insert(enemy_types);
        let player: PlayerData = serde_any::from_file_stem(level_folder.join("player"))
            .expect("Uh oh, could not load player data. Now crashing!!");
        crate::scenes::level::create_player(self, player);
        Ok(())
    }
}
