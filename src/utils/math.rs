pub use euclid::point2;
pub use euclid::vec2;

pub type Vector2 = euclid::Vector2D<f32>;

/// Create a vector with a magnitude of one from angle (in degrees)
pub fn angle_to_vector(angle: f32) -> Vector2 {
    vec2(angle.to_radians().cos(), angle.to_radians().sin())
}

/// Create a vector with a magnitude of length from angle
pub fn angle_length_to_vector(angle: f32, length: f32) -> Vector2 {
    angle_to_vector(angle) * length
}

/// Calculate the distance between two given points
pub fn distance_between_points(p1: Vector2, p2: Vector2) -> f32 {
    ((p2.x - p1.x).powi(2) + (p2.y - p1.y).powi(2)).sqrt()
}

/// Direction from a point to another point
pub fn direction_to_point(from: Vector2, to: Vector2) -> Vector2 {
    (to - from).normalize()
}

/// Move towards a point by speed
pub fn move_towards_point(from: Vector2, to: Vector2, speed: f32) -> Vector2 {
    let dir = direction_to_point(from, to);
    from + (dir * speed)
}
