pub mod math;

pub use math::Vector2;

/// Basic logging setup to log to the console with `simple_logger`. If `RUST_BACKTRACE` is set, outputs debug info and writes trace info.
/// If `RUST_BACKTRACE` is set to `full` then it outputs trace info on console as well.
pub fn setup_logging() {
    use simplelog::{CombinedLogger, Config, LevelFilter, TermLogger, TerminalMode, WriteLogger};
    use std::env::var;
    use std::fs::File;

    let levels = if let Ok(val) = var("RUST_BACKTRACE") {
        if val == "full" {
            (LevelFilter::Trace, LevelFilter::Trace)
        } else {
            (LevelFilter::Debug, LevelFilter::Trace)
        }
    } else {
        (LevelFilter::Info, LevelFilter::Debug)
    };

    let mut config = Config::default();
    config.filter_ignore = Some(&["gfx", "ggez::graphics"]);
    CombinedLogger::init(vec![
        TermLogger::new(levels.0, config, TerminalMode::Mixed).unwrap(),
        WriteLogger::new(levels.1, config, File::create("danmaku.log").unwrap()),
    ])
    .unwrap();
}
