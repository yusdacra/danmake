//! specs systems.
use crate::components::*;
use crate::types;
use crate::user_types;
use rayon::iter::ParallelIterator;
use specs::{self, Builder, Join, ParJoin};

pub struct MoveVelocity;

impl<'a> specs::System<'a> for MoveVelocity {
    type SystemData = (
        specs::WriteStorage<'a, Bounds>,
        specs::ReadStorage<'a, Motion>,
    );

    fn run(&mut self, (mut bounds, motions): Self::SystemData) {
        (&mut bounds, &motions).par_join().for_each(|(bd, mot)| {
            bd.center += mot.0;
        });
    }
}

pub struct BulletRemove;

impl<'a> specs::System<'a> for BulletRemove {
    type SystemData = (
        specs::ReadStorage<'a, Bounds>,
        specs::ReadStorage<'a, PlayerBullet>,
        specs::ReadStorage<'a, EnemyBullet>,
        specs::Entities<'a>,
    );

    fn run(&mut self, (bounds, player_bullets, enemy_bullets, entities): Self::SystemData) {
        (&bounds, &player_bullets, &entities)
            .par_join()
            .for_each(|(bd, _, entity)| {
                if bd.center.x < 0.0
                    || bd.center.x > 800.0
                    || bd.center.y > 600.0
                    || bd.center.y < 0.0
                {
                    entities.delete(entity).unwrap();
                }
            });
        (&bounds, &enemy_bullets, &entities)
            .par_join()
            .for_each(|(bd, _, entity)| {
                if bd.center.x < 0.0
                    || bd.center.x > 800.0
                    || bd.center.y > 600.0
                    || bd.center.y < 0.0
                {
                    entities.delete(entity).unwrap();
                }
            });
    }
}

pub struct CollideBullets;

impl<'a> specs::System<'a> for CollideBullets {
    type SystemData = (
        specs::ReadStorage<'a, PlayerBullet>,
        specs::ReadStorage<'a, EnemyBullet>,
        specs::ReadStorage<'a, Bounds>,
        specs::WriteStorage<'a, Player>,
        specs::WriteStorage<'a, Enemy>,
        specs::Entities<'a>,
    );

    fn run(
        &mut self,
        (
			player_bullets,
			enemy_bullets,
			bounds,
			mut player,
			mut enemies,
			entities,
		): Self::SystemData,
    ) {
        use specs::WorldExt;
        // Collide player bullets with enemies
        for (pb, pb_bd, pb_entity) in (&player_bullets, &bounds, &entities).join() {
            for (enemy, enemy_bd, enemy_entity) in (&mut enemies, &bounds, &entities).join() {
                if pb_bd.overlaps(&enemy_bd) {
                    entities.delete(pb_entity).unwrap();
                    if enemy.lives.checked_sub(pb.damage).unwrap_or(0) > 0 {
                        enemy.lives -= pb.damage;
                    } else {
                        entities.delete(enemy_entity).unwrap();
                        for player in (&mut player).join() {
                            player.score += enemy.score_gain;
                        }
                    }
                }
            }
        }
        // Collide enemy bullets with player
        for (pb, pb_bd, pb_entity) in (&enemy_bullets, &bounds, &entities).join() {
            for (player, player_bd, player_entity) in (&mut player, &bounds, &entities).join() {
                if pb_bd.overlaps(&player_bd) {
                    entities.delete(pb_entity).unwrap();
                    if player.lives.checked_sub(pb.damage).unwrap_or(0) > 0 {
                        player.lives -= pb.damage;
                    } else {
                        entities.delete(player_entity).unwrap();
                    }
                }
            }
        }
    }
}

#[derive(Default)]
pub struct MaintainLevel {
    level_timer: types::Timer,
}

impl<'a> specs::System<'a> for MaintainLevel {
    type SystemData = (
        specs::Write<'a, user_types::Layout>,
        specs::Read<'a, user_types::EnemyTypes>,
        specs::Read<'a, specs::LazyUpdate>,
        specs::Read<'a, types::SpriteMap>,
        specs::Read<'a, types::SpriteHandler>,
        specs::Entities<'a>,
    );

    fn run(
        &mut self,
        (mut layout, enemy_data_map, lazyupdate, spritemap, sprhandler, entities): Self::SystemData,
    ) {
        // Spawn enemies if their time has come
        if let Some(enemy_datas) = layout.remove(&self.level_timer.get_time_secs()) {
            for (origin, enemy_name, pattern) in enemy_datas {
                let enemy_data = match enemy_data_map.get(&enemy_name) {
                    Some(ed) => ed.clone(),
                    None => {
                        log::warn!("Enemy type with name {} not found", enemy_name);
                        continue;
                    }
                };
                let sprid = *spritemap.get(&enemy_data.sprite).unwrap();
                let sprsize = sprhandler.get(&sprid).0;
                for pos in pattern.patternize(origin.into()) {
                    lazyupdate
                        .create_entity(&entities)
                        .with(Enemy {
                            lives: enemy_data.lives,
                            score_gain: enemy_data.score_to_gain,
                        })
                        .with(ActionQueue::new(enemy_data.actions.clone()))
                        .with(sprid)
                        .with(Bounds::new(
                            pos.into(),
                            enemy_data.hitbox_size.unwrap_or(sprsize.into()).into(),
                        ))
                        .with(Motion::default())
                        .build();
                }
                log::trace!("spawned enemy");
            }
        }
        self.level_timer.tick();
    }
}

pub struct ExecuteAction;

impl<'a> specs::System<'a> for ExecuteAction {
    type SystemData = (
        specs::WriteStorage<'a, ActionQueue>,
        specs::WriteStorage<'a, Bounds>,
        specs::Read<'a, specs::LazyUpdate>,
        specs::Entities<'a>,
        specs::ReadStorage<'a, SpriteIdx>,
        specs::Read<'a, types::SpriteMap>,
        specs::Read<'a, types::SpriteHandler>,
        specs::Read<'a, user_types::BulletTypes>,
    );

    fn run(
        &mut self,
        (
            mut queues,
            mut bounds,
            lazyupdate,
            entities,
            sprites,
            spritemap,
            spritehandler,
            bullettypes,
        ): Self::SystemData,
    ) {
        use crate::utils::math::{angle_length_to_vector, distance_between_points, vec2};
        use user_types::ActionType::*;
        (&mut queues, &mut bounds, &sprites)
            .par_join()
            .for_each(|(action_queue, bd, sprite)| {
                action_queue.update();
                for (mut action, timer) in action_queue
                    .mut_curacts()
                    .drain(..)
                    .collect::<Vec<(user_types::Action, types::Timer)>>()
                {
                    // Convert "actions with relative variables" to "actions with absolute variables" (or primitives)
                    match action.0 {
                        Fire(offset, shot) => {
                            let spr_size = spritehandler.get(sprite).0;
                            let offset_vec = offset.as_vector();
                            action.0 = FireAt(
                                (bd.center
                                    + vec2(spr_size.x * offset_vec.x, spr_size.y * offset_vec.y))
                                .into(),
                                shot,
                            )
                        }
                        _ => (),
                    };
                    // Execute the action
                    match action.0 {
                        MoveTo(x, y) => {
                            if action.1 > 0.0 {
                                let point = vec2(x, y);
                                let dir = (point - bd.center).normalize();
                                let mag = distance_between_points(bd.center, point)
                                    / (action.1 - timer.get_time()) as f32;
                                let moveby = dir * types::DESIRED_DELTA as f32 * mag;
                                if !moveby.x.is_nan() && !moveby.y.is_nan() {
                                    bd.center += moveby;
                                }
                            } else {
                                bd.center = vec2(x, y);
                            }
                        }
                        MoveBy(dx, dy) => {
                            if action.1 > 0.0 {
                                let moveby = vec2(
                                    (dx / (action.1 - timer.get_time()) as f32)
                                        * types::DESIRED_DELTA as f32,
                                    (dy / (action.1 - timer.get_time()) as f32)
                                        * types::DESIRED_DELTA as f32,
                                );
                                if !moveby.x.is_nan() && !moveby.y.is_nan() {
                                    bd.center += moveby;
                                    action.0 = MoveBy(dx - moveby.x, dy - moveby.y);
                                }
                            } else {
                                bd.center += vec2(dx, dy);
                            }
                        }
                        FireAt(spawnpos, ref shot) => {
                            let (bullet_name, pattern, direction, speed, bpt) = shot;
                            let bullet_properties = bullettypes.get(bullet_name).unwrap();
                            let sprid = *spritemap.get(&bullet_properties.sprite).unwrap();
                            if timer.get_ticks() % *bpt as u64 == 0 {
                                for pos in pattern.clone().patternize(spawnpos.into()) {
                                    lazyupdate
                                        .create_entity(&entities)
                                        .with(EnemyBullet {
                                            damage: bullet_properties.damage,
                                        })
                                        .with(sprid)
                                        .with(Motion(angle_length_to_vector(
                                            *direction,
                                            *speed * types::DESIRED_DELTA as f32,
                                        )))
                                        .with(Bounds::new(
                                            pos,
                                            bullet_properties.hitbox_size.unwrap().into(),
                                        ))
                                        .build();
                                }
                            }
                        }
                        _ => panic!(),
                    }
                    // Only push the action to current actions again if the timer still hasn't passed duration
                    if action.1 > 0.0 && timer.get_time() < action.1 {
                        action_queue.mut_curacts().push((action, timer));
                    }
                }
            });
    }
}
