//! Anything that is deserialized goes here, as it will be used in the public API.
use crate::utils::math::Vector2;
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Clone, Deserialize)]
pub struct Action(pub ActionType, pub f64);

#[derive(Clone, Debug, Deserialize)]
pub enum ActionType {
    MoveTo(f32, f32),
    FireAt((f32, f32), ShotData),
    MoveBy(f32, f32),
    Fire(Offset, ShotData),
}

/// Offsets relative to a sprites origin (which is top left) (since it's relative to sprite origin, it takes sprites size into account)
#[derive(Debug, Clone, Copy, Deserialize)]
pub enum Offset {
    TopLeft,          // (0.0, 0.0)
    TopMiddle,        // (0.5, 0.0)
    TopRight,         // (1.0, 0.0)
    MiddleLeft,       // (0.0, -0.5)
    Middle,           // (0.5, -0.5)
    MiddleRight,      // (1.0, -0.5)
    BottomLeft,       // (0.0, -1.0)
    BottomMiddle,     // (0.5, -1.0)
    BottomRight,      // (1.0, -1.0)
    Custom(f32, f32), // Positions should be between [-1.0, 1.0].
}

impl Offset {
    pub fn as_vector(self) -> Vector2 {
        use crate::user_types::Offset::*;
        use crate::utils::math::vec2;
        match self {
            TopLeft => vec2(0.0, 0.0),
            TopMiddle => vec2(0.5, 0.0),
            TopRight => vec2(1.0, 0.0),
            MiddleLeft => vec2(0.0, -0.5),
            Middle => vec2(0.5, -0.5),
            MiddleRight => vec2(1.0, -0.5),
            BottomLeft => vec2(0.0, -1.0),
            BottomMiddle => vec2(0.5, -1.0),
            BottomRight => vec2(1.0, -1.0),
            Custom(x, y) => vec2(x, y),
        }
    }
}

/// Metadata of a level. This includes; level's name, creator's name, how "hard" it is etc.
#[derive(Debug, Clone, Deserialize)]
pub struct LevelMetadata {
    pub name: Box<str>,
    pub creator: Box<str>,
}

impl std::fmt::Display for LevelMetadata {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Level name: {}, Level creator: {}",
            self.name, self.creator
        )
    }
}

/// Data that is persistent through the level
#[derive(Debug, Clone, Deserialize)]
pub struct LevelData {
    pub bg_sprite: Option<Box<str>>,
    pub player_hitbox_sprite: Option<Box<str>>,
}

/// A hash map which contains name and position of enemy(s) and at what time they will spawn.
pub type Layout = HashMap<u64, Vec<((f32, f32), Box<str>, Pattern)>>;

/// Which name is equal to which enemy?
pub type EnemyTypes = HashMap<Box<str>, EnemyData>;

/// Which name is equal to which bullet?
pub type BulletTypes = HashMap<Box<str>, BulletData>;

/// Actions to be stored in queues, and then executed by their respective entity.
/// They are executed when the timer hits the time (in seconds).
pub type ActionLayout = HashMap<u64, Vec<Action>>;

/// Alias for required data to shoot a bullet (bullet name, pattern, direction to shoot, speed, bullet per second)
pub type ShotData = (Box<str>, Pattern, f32, f32, u8);

/// Contains data required to spawn an enemy.
#[derive(Debug, Clone, Deserialize)]
pub struct EnemyData {
    pub score_to_gain: u64,
    pub lives: u64,
    pub actions: ActionLayout,
    pub sprite: Box<str>,
    pub hitbox_size: Option<(f32, f32)>,
}

/// Contains data required for a bullet.
#[derive(Debug, Clone, Deserialize)]
pub struct BulletData {
    pub hitbox_size: Option<(f32, f32)>,
    pub sprite: Box<str>,
    pub damage: u64,
}

/// Contains data required to spawn a player.
#[derive(Debug, Clone, Deserialize)]
pub struct PlayerData {
    pub spawn_position: (f32, f32),
    pub lives: u64,
    pub max_lives: u64,
    pub speed: f32,
    pub sprite: Box<str>,
    pub hitbox_size: Option<(f32, f32)>,
}

/// Public wrapper over core `Pattern` types.
#[derive(Debug, Clone, Deserialize)]
pub enum Pattern {
    Line(usize, f32, f32),
    Circle(usize, f32),
    Single,
}

impl Pattern {
    pub fn patternize(self, origin: Vector2) -> Vec<Vector2> {
        use crate::core::pattern::{self, Pattern};
        use crate::user_types::Pattern::*;
        match self {
            Line(count, dir, length) => pattern::Line::new(count, dir, length).patternize(origin),
            Circle(count, radius) => pattern::Circle::new(count, radius).patternize(origin),
            Single => pattern::Single.patternize(origin),
        }
    }
}
