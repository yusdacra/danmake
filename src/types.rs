use std::collections::HashMap;

use crate::utils::Vector2;
use ggez::graphics::Image;

pub const DESIRED_FPS: u32 = 60;
pub const DESIRED_DELTA: f64 = 1.0 / 60.0;

#[derive(Clone, Debug, Default)]
/// Timer based on ticks. To get time as float, it multiplies ticks with `DESIRED_DELTA`.
pub struct Timer {
    ticks: u64,
}

impl Timer {
    pub fn new(ticks: u64) -> Self {
        Timer { ticks }
    }
    pub fn zero() -> Self {
        Self::new(0)
    }
    #[inline]
    pub fn tick(&mut self) {
        self.ticks += 1;
    }
    pub fn reset(&mut self) {
        self.ticks = 0;
    }
    #[inline]
    pub fn get_time(&self) -> f64 {
        self.ticks as f64 * crate::types::DESIRED_DELTA
    }
    #[inline]
    pub fn get_time_secs(&self) -> u64 {
        self.get_time() as u64
    }
    #[inline]
    pub fn get_ticks(&self) -> u64 {
        self.ticks
    }
}

/// Hashmap for matching sprite names with sprite ids.
pub type SpriteMap = HashMap<Box<str>, crate::components::SpriteIdx>;

/// A Vec that stores every sprite.
/// You can't remove a sprite from it, you can only clear the Vec, this is to prevent sprite ids from becoming invalid.
#[derive(Debug, Clone)]
pub struct SpriteHandler {
    pool: Vec<(Vector2, Image)>,
}

impl SpriteHandler {
    pub fn new() -> Self {
        Self { pool: Vec::new() }
    }

    pub fn add(&mut self, img: Image) -> crate::components::SpriteIdx {
        self.pool.push((
            {
                let d = img.dimensions();
                Vector2::new(d.w, d.h)
            },
            img,
        ));
        crate::components::SpriteIdx(self.pool.len() - 1)
    }

    pub fn clear(&mut self) {
        self.pool.clear()
    }

    #[inline(always)]
    pub fn get(&self, id: &crate::components::SpriteIdx) -> &(Vector2, Image) {
        &self.pool[id.0]
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.pool.len()
    }
}

impl Default for SpriteHandler {
    fn default() -> Self {
        Self::new()
    }
}
