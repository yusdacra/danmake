use crate::utils::math::{angle_length_to_vector, vec2, Vector2};

/// Trait for something that can calculate a pattern from a given origin.
pub trait Pattern {
    fn patternize(&self, origin: Vector2) -> Vec<Vector2>;
}

#[derive(Debug, Clone)]
pub struct Line {
    count: usize,
    direction: f32,
    length: f32,
}

impl Line {
    pub fn new(count: usize, direction: f32, length: f32) -> Self {
        Self {
            count,
            direction,
            length,
        }
    }
}

impl Pattern for Line {
    fn patternize(&self, origin: Vector2) -> Vec<Vector2> {
        let mut result = Vec::with_capacity(self.count);
        let gap = self.length / self.count as f32;
        for i in 0..self.count {
            result.push(origin + angle_length_to_vector(self.direction, gap * i as f32));
        }
        result
    }
}

#[derive(Debug, Clone)]
pub struct Circle {
    count: usize,
    radius: f32,
}

impl Circle {
    pub fn new(count: usize, radius: f32) -> Self {
        Self { count, radius }
    }
}

impl Pattern for Circle {
    fn patternize(&self, origin: Vector2) -> Vec<Vector2> {
        let mut result = Vec::with_capacity(self.count);
        let gap = 360.0 / self.count as f32;
        for i in 0..self.count {
            result.push(vec2(
                origin.x + self.radius * (gap * i as f32).to_radians().cos(),
                origin.y + self.radius * (gap * i as f32).to_radians().sin(),
            ));
        }
        result
    }
}

/// This is here just because that we will be using patterns everywhere.
/// Sometimes you don't need a pattern, so this fills that hole up.
#[derive(Debug, Clone, Default)]
pub struct Single;

impl Pattern for Single {
    fn patternize(&self, origin: Vector2) -> Vec<Vector2> {
        vec![origin]
    }
}
